# MDLCNM - Marcus Dimension as a Linear Combination of Normal Modes



Script is a part of publication: *Identifying the Marcus dimension of electron transfer from ab initio calculations*

An optimized geometry and normal coordinates from frequency 
calculation need to be provided. The geometry as an agrument.
The normal coordinates as "vib_normal_modes" file generated
by TURBOMOLE.
Data from the sampling method have to be saved in "wigner-properties.txt"
file. Strutcure of the file should be following: First lines that 
start with '#' are comments and will not be read. First column
contain identifiers of sampled geometries and will not be read.
Second column containts IVCT excitation energies in cm⁻¹.
Columns 3, 4 and 5 contain spin population of the first center,
the second center and the bridge, respectively. Columns 6 and 7
contain dipole moment and oscillator strength and will not be read.
The remaining 3N-6 columns contain geometry of the molecule in
normal coordinates.
    
Script then performs multi-component fit of normal coordinates
to both the excitation energy and the electron position obtained
from the spin populations. Resulting coefficient are subsequently 
used to construct the Marcus dimension as a linear combination
of vibrational modes. This will be saved as an unnormalized 
vector of Cartesian displacements.

Functionality of the script is tested with Julia 1.7.1, necessary packages can be installed by copying following into a Julia REPL:
```
julia> using Pkg
julia> Pkg.add(["Plots","ArgParse"])
```

File `wigner_properties.txt` is an example data file that needs to be provided by a user.
Use script with files `wigner_properties.txt` and `vib_normal_modes` in the working directory as:
```
julia MDLCNM.jl optimized-geometry.xyz
```
